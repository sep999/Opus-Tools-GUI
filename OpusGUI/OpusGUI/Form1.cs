﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
namespace OpusGUI
{
    public enum STATE
    {
        READY,
        CONVERTING,
        FINISHED
    }
    public partial class Form1 : Form
    {
       
        public Form1()
        {   
            InitializeComponent();
            comboBox_bitrate.SelectedIndex = 0;
            comboBox_bitratemode.SelectedIndex = 0;
        }
        /// <summary>
        /// 拖入文件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void list_files_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        /// <summary>
        /// 拖入文件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void list_files_DragDrop(object sender, DragEventArgs e)
        {
            String[] files = e.Data.GetData(DataFormats.FileDrop, false) as String[];
            add_file_to_list(files);
           
        }
        void add_file_to_list(string[] files)
        {
            int list_count = list_files.Items.Count;
            //Copy file from external application
            list_files.BeginUpdate();
            for (int i = 0; i < files.Length; i++)
            {
                if (check_files_extension(files[i]))
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = (list_count + i).ToString();
                    item.SubItems.Add(files[i]);
                    item.SubItems.Add(get_item_state_name(STATE.READY));
                    list_files.Items.Add(item);
                    //set_item_state(i, STATE.READY);
                }

            }
            list_files.EndUpdate();
        }
        /// <summary>
        /// 获取任务状态名称
        /// </summary>
        /// <param name="state">任务状态</param>
        /// <returns></returns>
        string get_item_state_name(STATE state)
        {
            string statename = "";
            switch (state)
            {
                case STATE.READY:
                    statename = "准备";
                    break;
                case STATE.CONVERTING:
                    statename = "转换中";
                    break;
                case STATE.FINISHED:
                    statename = "完成";
                    break;
            }
            return statename;
        }
        /// <summary>
        /// 获取列表状态
        /// </summary>
        /// <param name="index">任务序号</param>
        /// <returns></returns>
        STATE get_item_state(int index)
        {
            string statename = list_files.Items[index].SubItems[2].Text;
            STATE state= STATE.READY;
            switch (statename)
            {
                case "准备":
                    state = STATE.READY;
                    break;
                case "转换中":
                    state = STATE.CONVERTING;
                    break;
                case "完成":
                    state = STATE.FINISHED;
                    break;
                
            }
            return state;


        }
        /// <summary>
        /// 设置任务状态
        /// </summary>
        /// <param name="index">任务序号</param>
        /// <param name="state">任务状态</param>
        void set_item_state(int index,STATE state)
        {
            string statename = get_item_state_name(state);
            list_files.Items[index].SubItems[2].Text = statename;
        }
        /// <summary>
        /// 检查文件后缀名
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool check_files_extension(string filename)
        {
            string extension =Path.GetExtension(filename);
            extension = extension.ToLower();
            if(extension==".wav")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 开始转换按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_startconvert_Click(object sender, EventArgs e)
        {

            Convert();
        }
        /// <summary>
        /// opus返回事件
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="index"></param>
        private void Opus_opusEvent(string callback,int index)
        {
            logbox.Invoke(new EventHandler(delegate
            {
                if(index!=-1)
                {
                    if(callback=="完成")
                    {
                        set_item_state(index, STATE.FINISHED);
                    }
                    logbox.AppendText(callback + " #" + index.ToString());
                    logbox.AppendText(Environment.NewLine);
                }
                else
                {
                    logbox.AppendText(callback);
                    logbox.AppendText(Environment.NewLine);
                }
                

            }));
        }
        /// <summary>
        /// 开始转换
        /// </summary>
        private void Convert()
        {
            Opus_opusEvent("开始转换",-1);

            if (list_files.Items.Count>0)
            {
                Opus opus = new Opus();
                opus.opusEvent += Opus_opusEvent;
                opus._convertlist.Clear();
                for (int i = 0; i < list_files.Items.Count; i++)
                {
                    if(list_files.Items[i].SubItems[2].Text=="准备")
                        opus._convertlist.Add(int.Parse(list_files.Items[i].Text),list_files.Items[i].SubItems[1].Text);
                }
                opus.addition_args = ReadConfig();
                Thread t = new Thread(new ThreadStart(opus.Convert));
                t.IsBackground = true;
                t.Start();
            }

        }
        /// <summary>
        /// 读取配置
        /// </summary>
        /// <returns></returns>
        private string ReadConfig()
        {
            string args = " ";
            if(comboBox_bitrate.SelectedIndex>0)
            {
                args = args+"--bitrate " + comboBox_bitrate.SelectedItem.ToString()+" ";
            }
            switch (comboBox_bitratemode.SelectedIndex)
            {
                case 0:
                    args = args + "--vbr ";
                    break;
                case 1:
                    args = args + "--cbr ";
                    break;
                case 2:
                    args = args + "--hard-cbr ";
                    break;
            }

            
            return args;
        }

        /// <summary>
        /// logbox文本改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logbox_TextChanged(object sender, EventArgs e)
        {
            logbox.ScrollToCaret();
            logbox.Select(logbox.TextLength, 0);
        }

        private void menu_logbox_clear_Click(object sender, EventArgs e)
        {
            logbox.Clear();
        }

        private void menu_clear_Click(object sender, EventArgs e)
        {
            list_files.Clear();
        }

        private void menu_delete_Click(object sender, EventArgs e)
        {
            if(list_files.SelectedItems.Count>0)
            {
                foreach(int i in list_files.SelectedIndices)
                {
                    list_files.Items.RemoveAt(i);
                }
                for(int i=0;i<list_files.Items.Count;i++)
                {
                    list_files.Items[i].Text = i.ToString();
                }
                
            }
        }

        private void menu_import_Click(object sender, EventArgs e)
        {
            if(OPENdialog.ShowDialog()==DialogResult.OK)
            {
                add_file_to_list(OPENdialog.FileNames);
            }
        }

        private void menu_setready_Click(object sender, EventArgs e)
        {
            if(list_files.SelectedItems.Count>0)
            {
                foreach(int index in list_files.SelectedIndices)
                {
                    set_item_state(index, STATE.READY);
                }
            }
            
        }

        private void menu_setfinished_Click(object sender, EventArgs e)
        {
            if (list_files.SelectedItems.Count > 0)
            {
                foreach (int index in list_files.SelectedIndices)
                {
                    set_item_state(index, STATE.FINISHED);
                }
            }
        }
    }
}
