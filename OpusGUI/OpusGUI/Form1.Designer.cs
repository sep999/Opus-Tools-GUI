﻿namespace OpusGUI
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.list_files = new System.Windows.Forms.ListView();
            this.taskID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.item_filename = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.item_status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_startconvert = new System.Windows.Forms.Button();
            this.comboBox_bitratemode = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_bitrate = new System.Windows.Forms.ComboBox();
            this.logbox = new System.Windows.Forms.RichTextBox();
            this.list_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.logbox_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_delete = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_clear = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_import = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_logbox_clear = new System.Windows.Forms.ToolStripMenuItem();
            this.OPENdialog = new System.Windows.Forms.OpenFileDialog();
            this.menu_setready = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_setfinished = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.list_menu.SuspendLayout();
            this.logbox_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // list_files
            // 
            this.list_files.AllowDrop = true;
            this.list_files.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.list_files.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.taskID,
            this.item_filename,
            this.item_status});
            this.list_files.ContextMenuStrip = this.list_menu;
            this.list_files.FullRowSelect = true;
            this.list_files.Location = new System.Drawing.Point(12, 12);
            this.list_files.Name = "list_files";
            this.list_files.Size = new System.Drawing.Size(597, 320);
            this.list_files.TabIndex = 0;
            this.list_files.UseCompatibleStateImageBehavior = false;
            this.list_files.View = System.Windows.Forms.View.Details;
            this.list_files.DragDrop += new System.Windows.Forms.DragEventHandler(this.list_files_DragDrop);
            this.list_files.DragEnter += new System.Windows.Forms.DragEventHandler(this.list_files_DragEnter);
            // 
            // taskID
            // 
            this.taskID.Text = "任务ID";
            // 
            // item_filename
            // 
            this.item_filename.Text = "文件列表";
            this.item_filename.Width = 450;
            // 
            // item_status
            // 
            this.item_status.Text = "状态";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btn_startconvert);
            this.groupBox1.Controls.Add(this.comboBox_bitratemode);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox_bitrate);
            this.groupBox1.Location = new System.Drawing.Point(615, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(190, 440);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设置";
            // 
            // btn_startconvert
            // 
            this.btn_startconvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_startconvert.Location = new System.Drawing.Point(8, 411);
            this.btn_startconvert.Name = "btn_startconvert";
            this.btn_startconvert.Size = new System.Drawing.Size(75, 23);
            this.btn_startconvert.TabIndex = 4;
            this.btn_startconvert.Text = "开始转换";
            this.btn_startconvert.UseVisualStyleBackColor = true;
            this.btn_startconvert.Click += new System.EventHandler(this.btn_startconvert_Click);
            // 
            // comboBox_bitratemode
            // 
            this.comboBox_bitratemode.FormatString = "N2";
            this.comboBox_bitratemode.FormattingEnabled = true;
            this.comboBox_bitratemode.Items.AddRange(new object[] {
            "VBR",
            "CBR",
            "Hard CBR"});
            this.comboBox_bitratemode.Location = new System.Drawing.Point(84, 53);
            this.comboBox_bitratemode.Name = "comboBox_bitratemode";
            this.comboBox_bitratemode.Size = new System.Drawing.Size(77, 20);
            this.comboBox_bitratemode.TabIndex = 3;
            this.comboBox_bitratemode.Text = "vbr";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "码率模式";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "码率";
            // 
            // comboBox_bitrate
            // 
            this.comboBox_bitrate.FormatString = "N2";
            this.comboBox_bitrate.FormattingEnabled = true;
            this.comboBox_bitrate.Items.AddRange(new object[] {
            "默认",
            "48",
            "96",
            "192",
            "384",
            "510"});
            this.comboBox_bitrate.Location = new System.Drawing.Point(84, 20);
            this.comboBox_bitrate.Name = "comboBox_bitrate";
            this.comboBox_bitrate.Size = new System.Drawing.Size(77, 20);
            this.comboBox_bitrate.TabIndex = 0;
            // 
            // logbox
            // 
            this.logbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logbox.ContextMenuStrip = this.logbox_menu;
            this.logbox.Location = new System.Drawing.Point(12, 338);
            this.logbox.Name = "logbox";
            this.logbox.Size = new System.Drawing.Size(597, 114);
            this.logbox.TabIndex = 2;
            this.logbox.Text = "";
            this.logbox.TextChanged += new System.EventHandler(this.logbox_TextChanged);
            // 
            // list_menu
            // 
            this.list_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_import,
            this.menu_delete,
            this.menu_clear,
            this.menu_setready,
            this.menu_setfinished});
            this.list_menu.Name = "list_menu";
            this.list_menu.Size = new System.Drawing.Size(173, 114);
            // 
            // logbox_menu
            // 
            this.logbox_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_logbox_clear});
            this.logbox_menu.Name = "logbox_menu";
            this.logbox_menu.Size = new System.Drawing.Size(101, 26);
            // 
            // menu_delete
            // 
            this.menu_delete.Name = "menu_delete";
            this.menu_delete.Size = new System.Drawing.Size(172, 22);
            this.menu_delete.Text = "删除";
            this.menu_delete.Click += new System.EventHandler(this.menu_delete_Click);
            // 
            // menu_clear
            // 
            this.menu_clear.Name = "menu_clear";
            this.menu_clear.Size = new System.Drawing.Size(172, 22);
            this.menu_clear.Text = "清空";
            this.menu_clear.Click += new System.EventHandler(this.menu_clear_Click);
            // 
            // menu_import
            // 
            this.menu_import.Name = "menu_import";
            this.menu_import.Size = new System.Drawing.Size(172, 22);
            this.menu_import.Text = "导入";
            this.menu_import.Click += new System.EventHandler(this.menu_import_Click);
            // 
            // menu_logbox_clear
            // 
            this.menu_logbox_clear.Name = "menu_logbox_clear";
            this.menu_logbox_clear.Size = new System.Drawing.Size(100, 22);
            this.menu_logbox_clear.Text = "清空";
            this.menu_logbox_clear.Click += new System.EventHandler(this.menu_logbox_clear_Click);
            // 
            // OPENdialog
            // 
            this.OPENdialog.DefaultExt = "wav";
            this.OPENdialog.Filter = "WAV文件|*.wav";
            this.OPENdialog.Multiselect = true;
            this.OPENdialog.Title = "导入wav音频";
            // 
            // menu_setready
            // 
            this.menu_setready.Name = "menu_setready";
            this.menu_setready.Size = new System.Drawing.Size(172, 22);
            this.menu_setready.Text = "设置该任务为准备";
            this.menu_setready.Click += new System.EventHandler(this.menu_setready_Click);
            // 
            // menu_setfinished
            // 
            this.menu_setfinished.Name = "menu_setfinished";
            this.menu_setfinished.Size = new System.Drawing.Size(172, 22);
            this.menu_setfinished.Text = "设置该任务为完成";
            this.menu_setfinished.Click += new System.EventHandler(this.menu_setfinished_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 464);
            this.Controls.Add(this.logbox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.list_files);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "OpusGUI";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.list_menu.ResumeLayout(false);
            this.logbox_menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView list_files;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox logbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_bitrate;
        private System.Windows.Forms.ColumnHeader taskID;
        private System.Windows.Forms.ColumnHeader item_filename;
        private System.Windows.Forms.ColumnHeader item_status;
        private System.Windows.Forms.ComboBox comboBox_bitratemode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_startconvert;
        private System.Windows.Forms.ContextMenuStrip list_menu;
        private System.Windows.Forms.ToolStripMenuItem menu_import;
        private System.Windows.Forms.ToolStripMenuItem menu_delete;
        private System.Windows.Forms.ToolStripMenuItem menu_clear;
        private System.Windows.Forms.ContextMenuStrip logbox_menu;
        private System.Windows.Forms.ToolStripMenuItem menu_logbox_clear;
        private System.Windows.Forms.OpenFileDialog OPENdialog;
        private System.Windows.Forms.ToolStripMenuItem menu_setready;
        private System.Windows.Forms.ToolStripMenuItem menu_setfinished;
    }
}

