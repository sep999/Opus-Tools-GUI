﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OpusGUI
{
    public delegate void OpusEventHandler(string callback,int index);
    public class Opus
    {
        
        private string _args;
        public string addition_args;
        public Dictionary<int,string> _convertlist = new Dictionary<int, string>();
        private string _callback;
        public event OpusEventHandler opusEvent;

       
        public string args
        {
            set { _args = value; }
            get { return _args; }
        }

        public string Callback
        {
            get
            {
                return _callback;
            }

            set
            {
                _callback = value;
                opusEvent(_callback,-1);
            }
        }
        /// <summary>
        /// 开始转换事件
        /// </summary>
        public async void Convert()
        {
            
            foreach(int i in _convertlist.Keys)
            { 
                
                string path = Path.GetDirectoryName(_convertlist[i]);
                string newname = Path.GetFileNameWithoutExtension(_convertlist[i]) + ".opus";
                args = addition_args + " \"" + _convertlist[i] + "\" \"" +path+"\\"+newname+"\"";
                await Task.Run(()=>Run_enc());
                opusEvent("完成",i);
            }
            this.Callback = "任务完成";
        }
       
        void Run_enc()
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();

            p.StartInfo.FileName = "opusenc.exe";
            p.StartInfo.Arguments = _args;
            Console.WriteLine(_args);
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = true;//不显示程序窗口

            p.Start();//启动程序
            //callback.Invoke(new EventHandler(delegate
            //{
            //    callback.AppendText("start mission: " + fn);
            //    callback.AppendText(Environment.NewLine);
            //}));
            //向cmd窗口发送输入信息
            // p.StandardInput.WriteLine(filename+" -b");

            p.StandardInput.AutoFlush = true;
            // p.StandardInput.WriteLine("exit");
            //向标准输入写入要执行的命令。这里使用&是批处理命令的符号，表示前面一个命令不管是否执行成功都执行后面(exit)命令，如果不执行exit命令，后面调用ReadToEnd()方法会假死
            //同类的符号还有&&和||前者表示必须前一个命令执行成功才会执行后面的命令，后者表示必须前一个命令执行失败才会执行后面的命令



            //获取cmd窗口的输出信息
            string output = p.StandardOutput.ReadLine();

            StreamReader reader = p.StandardOutput;
            string line = reader.ReadLine();
            while (!reader.EndOfStream)
            {
                //str += line + "  ";
                line = reader.ReadLine();
                this.Callback = line;
                //logbox.Invoke(new EventHandler(delegate
                //{
                //    logbox.AppendText(line);
                //    logbox.AppendText(Environment.NewLine);

                //}));
            }
            p.Close();
        }
        
    }
   

   
}
